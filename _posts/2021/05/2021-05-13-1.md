---
layout: post
title: "LineageOS 설치"
---

## TWRP 설치

* Google Nexus 5
  * adb 접속이 가능하도록 개발자 설정을 한다.

  * bootloader 모드로 리부팅한다.
  ```
  adb reboot bootloader
  ```
  * fastboot 기능이 작동하는지 확인한다.
  ```
  fastboot devices
  ```
  * twrp 파일( [twrp-3.6.0_9-0-hammerhead.img](https://dl.twrp.me/hammerhead/twrp-3.6.0_9-0-hammerhead.img) )을 설치한다.
  ```
  fastboot flash recovery twrp-3.6.0_9-0-hammerhead.img
  ```

* Samsung Galaxy S7 (Edge)
  * [Odin v3.10.7](https://samsungodin.com/download/Odin3_v3.10.7.zip)을 이용하여 설치한다.
  * Galaxy S7 : [twrp-3.6.0_9-0-herolte.img.tar](https://dl.twrp.me/herolte/twrp-3.6.0_9-0-herolte.img.tar)
  * Galaxy S7 Edge : [twrp-3.7.0_9-1-hero2lte.img.tar](https://dl.twrp.me/hero2lte/twrp-3.7.0_9-1-hero2lte.img.tar)
  * 참고 : [https://youtu.be/-xStocRoyvk](https://youtu.be/-xStocRoyvk)


* Huawei Nexus 6P
  * https://wiki.lineageos.org/devices/angler/install
  * twrp 파일( [twrp-3.7.0_9-0-angler.img](https://dl.twrp.me/angler/twrp-3.7.0_9-0-angler.img) )을 설치한다.

## OS, Rooting, Google Apps 설치

* Recovery mode로 부팅을 한다.
* TWRP - Wipe를 선택하여 모든 내용을 삭제한다(Advanced Wipe에서 전부 선택한다).
* TWRP - Wipe - Format Data 를 선택하여 포맷한다.
* OS, Rooting, Google Apps 순서로 zip 파일을 설치(adb sideload <파일명>)한다.
* OS와 Rooting을 설치하고 나서 부팅을 하면 제일 처음 부팅은 5분 정도 소요된다.
* 이후 Settings - Developer options - Root access에서 "Apps and ADB"를 선택하여 루팅 access를 할 수 있도록 한다.

* Google Nexus 5(ARM)

  |Name|Zip file|URL|md5sum|
  |--|--|--|--|
  |OS|lineage-14.1-20190126-nightly-hammerhead-signed.zip|[Link](https://lineageosroms.com/hammerhead)|c6e88aca397d56f313773d927522dfcf|
  |Rooting|addonsu-14.1-arm-signed.zip|[Link](https://lineageosroms.com/extras/)|62d9aef3983ced8598e0d8f56a91b012|
  |Google Apps|open_gapps-arm-7.1-pico-20220215.zip|[Link](https://opengapps.org)|ff23028a1fcabbebec0bf3ccb133c0db|

* Samsung Galaxy S7(ARM64)

  |Name|Zip file|URL|md5sum|
  |--|--|--|--|
  |OS|lineage-14.1-20190224-nightly-herolte-signed.zip|[Link](https://lineageosroms.com/herolte)|7080908567f0e0ad7678345f90ae2523|
  |Rooting|addonsu-14.1-arm64-signed.zip|[Link](https://lineageosroms.com/extras/)|5c389032bac91f2d1f1ccd66668e5b76|
  |Google Apps|open_gapps-arm64-7.1-pico-20220215.zip|[Link](https://opengapps.org)|32cd41da00b7ec770f7411283e999574|

* Samsung Galaxy S7 Edge(ARM64)

  |Name|Zip file|URL|md5sum|
  |--|--|--|--|
  |OS|lineage-14.1-20190108-nightly-hero2lte-signed.zip|[Link](https://lineageosroms.com/hero2lte)|93e50b532e385befa8f6c3a550fb894d|
  |Rooting|addonsu-14.1-arm64-signed.zip|[Link](https://lineageosroms.com/extras/)|5c389032bac91f2d1f1ccd66668e5b76|
  |Google Apps|open_gapps-arm64-7.1-pico-20220215.zip|[Link](https://opengapps.org)|32cd41da00b7ec770f7411283e999574|
  
* Huawei Nexus 6P(ARM64)

  |Name|Zip file|URL|md5sum|
  |--|--|--|--|
  |OS|lineage-15.1-20200204-nightly-angler-signed.zip|[Link](https://lineageosroms.com/angler)|8ba439bcc30425f824e54ecfa25929df|
  |Rooting|addonsu-14.1-arm64-signed.zip|[Link](https://lineageosroms.com/extras/)|5c389032bac91f2d1f1ccd66668e5b76|
  |Google Apps|open_gapps-arm64-8.1-pico-20220215.zip|[Link](https://opengapps.org)|fb70384f0df80d94256ef6ef6b818f73|

* Rooting zip(addonsu...)이 제대로 설치가 되지 않는 경우
  * lineage-17.1-20201231-UNOFFICIAL-hammerhead.zip 파일과 같은 unofficial 파일을 설치하면 루팅 파일(addonsu...zip)이 제대로 설치되지 않을 수 있다.
  * addonsu...zip 파일을 설치할 때 "Installing su addon..."이라는 로그가 찍히는지 확인한다. 이 로그가 찍히지 않으면 su 파일이 복사가 안된 것이다(/system/xbin/su 혹은 /system/bin/su).
  * addonsu...zip 파일이 제대로 설치가 되지 않을 때에는 OS를 설치하고 우선 재부팅까지 해서 세팅을 완료한 이후 다시 Recovery mode로 진입하여 설치를 시도해 본다.
  * addonsu-remove-14.1-arm-signed.zip 파일로 제대로 삭제를 하고 다시 addonsu-14.1-arm-signed.zip 파일을 설치해 본다.

## References

[How to Install LineageOS on Android](https://www.howtogeek.com/348545/how-to-install-lineageos-on-android)

[4년전 폰이 최근폰보다 더 좋은거 실화? 안드로이드 폰, 리니지 OS 설치하기 + 루팅하기](https://youtu.be/masdP5K4oLU)

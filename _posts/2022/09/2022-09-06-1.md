---
layout: post
title: "Windows에서 ARP table을 static으로 설정하기"
---

Windows 상위 버전에서 "arp -s" 명령어를 이용하여 ARP cache를 정적(static)으로 설정을 하면 다음과 같이 에러가 날 수 있습니다.

![](/files/2022/09/arp_error.png)

<br>
이 경우 다음과 같은 방법으로 ARP cache를 static으로 설정할 수 있습니다. 우선 다음과 같은 명령어를 통해서 인터페이스 이름을 알아 냅니다.

```
netsh interface ip show config
```

![](/files/2022/09/netsh_interface.png)

<br>
알아낸 인터페이스 이름을 이용하여 ARP cache를 static으로 설정합니다.

```
netsh interface ipv4 add neighbors <인터페이스 이름> <IP> <MAC>
```

![](/files/2022/09/netsh_command.png)

<br>
ARP cache 설정이 제대로 되었는지 "arp -a" 명령어를 통하여 확인을 합니다.

```
arp -a
```

![](/files/2022/09/netsh_result.png)

<br>
ARP cache는 "arp -d" 혹은 "netsh interface ip delete arpcache" 명령어를 통해서 초기화를 할 수 있습니다.

```
arp -d
netsh interface ip delete arpcache
```

![](/files/2022/09/arp_delete.png)

<br>
## 출처
[http://www.gilgil.net/?document_srl=30286](http://www.gilgil.net/?document_srl=30286)



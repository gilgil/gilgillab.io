---
layout: post
title: "BoB 8기 오델로 게임 제작 후기"
---

### 오델로 게임 경연 대회
작년에 진행되었던 [BoB 7기 보안제품개발 트랙 오델로 경연 대회](/2019/02/21/1.html)를 BoB 8기에서도 진행을 하였습니다. 작년과 달라진 점은 플레이를 하고 나서 게임이 끝났을 때 돌의 숫자가 많은 쪽이 아닌 적은 쪽이 승리를 하는 것으로 룰을 바꾸었다는 것입니다.

<br>
### 게임 개발
작년과 마찬가지로 프로토콜을 설계하고 설계된 프로토콜에 따라 각자 프로그래밍을 진행하였습니다. 교육생들이 각자 만든 프로그램의 스크린샷을 공개합니다.

![](/files/2020/02/han-jong-ho.png)<br><br>
![](/files/2020/02/im-ji-hoon.png)<br><br>
![](/files/2020/02/ji-jae-hyeong.png)<br><br>
![](/files/2020/02/jo-jae-hyeon.png)<br><br>
![](/files/2020/02/kim-hyun-hong.png)<br><br>
![](/files/2020/02/kim-kyeon-woo.png)<br><br>
![](/files/2020/02/son-young-rak.PNG)<br><br>
![](/files/2020/02/won-jong-eun.png)<br><br>

<br>
### 대회 영상(유튜브)
<iframe width="560" height="315" src="https://www.youtube.com/embed/RWljrr2wYTk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>
### 후기
이번에는 저를 대신하여 [지용빈 교육생](https://www.facebook.com/profile.php?id=100002595891006)이 사회를 진행하였습니다. 이 자리를 빌어 감사의 말을 전합니다. [지용빈 교육생의 게임 준비 후기](https://github.com/Tempuss/BoB8_Dev_Othello_Contest)도 링크로 남깁니다.

원래 오델로 게임과 다르게 돌의 숫자가 적은 쪽이 승리하는 것으로 바꾸다 보니 기존에 인터넷에 공개되어 있던 코드를 가져다 사용하지 못하고, 학습에 필요한 기보들이 존재하지 않아 학습에 필요한 데이터를 스스로 만들어야 하는 어려움 등 이 있었습니다. 그럼에도 불구하고 한명의 포기도 없이 8명(김견우, 김현홍, 손영락, 원종은, 임지훈, 조재현, 지재형, 한종호) 모두 프로그램의 구현에 성공을 하였습니다. 고생한 교육생들에게 박수 짝짝짝~

![](/files/2020/02/othello_prepare.jpg)<br><br>
![](/files/2020/02/othello_players.jpg)<br><br>


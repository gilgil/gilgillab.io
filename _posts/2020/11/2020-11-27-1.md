---
layout: post
title: "Install ipTIME A2000UA-4dBi driver in linux"
---

국내 제품에서 monitor mode를 지원한다는 지인(BoB 7기 개발 트랙 이학인)의 정보를 얻어, ipTIME A2000UA-4dBi 어댑터를 구매하였다. 아래는 개봉기.

![](../../..//files/2020/11/iptime-A2000UA-4dBi-1.png)  
랜선은 왜 샀을까 후회 중.

![](../../..//files/2020/11/iptime-A2000UA-4dBi-2.png)  
포장지를 뜯어 보면 위와 같이 되어 있다. 아주 단순함.

![](../../..//files/2020/11/iptime-A2000UA-4dBi-3.png)  
설치 완료 화면. 이제 USB를 컴퓨터에 꽂고 돌려 보려고 하였으나...

Linux에서 인터페이스 인식을 하지 못한다. 해당 드라이버를 설치해 줘야만 사용할 수가 있다.

구글링해 보다가 aircrack-ng github 사이트에 정리가 잘 되어 있는 git repository를 발견함.

[https://github.com/aircrack-ng/rtl8812au](https://github.com/aircrack-ng/rtl8812au)

다음과 같은 명령어로 쉽게 설치가 완료된다.
```
$ sudo apt-get install dkms
$ git clone https://github.com/aircrack-ng/rtl8812au
$ cd rtl8812au
$ sudo make dkms_install
$ modprobe 88XXau
```

<iframe width="560" height="315" src="https://www.youtube.com/embed/KsXpEIEv6Cw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

v5.6.4.2에서 802.11 packet injection이 작동하지 않는 경우 다음과 같은 patch 작업을 하고 나서 코드를 재빌드하여 설치하여야 한다.  
[https://github.com/aircrack-ng/rtl8812au/issues/819](https://github.com/aircrack-ng/rtl8812au/issues/819)

<iframe width="560" height="315" src="https://www.youtube.com/embed/jq2-BU5C_PI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

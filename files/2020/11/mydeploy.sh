#!/bin/bash

if [ -z "$1" ]; then
	echo "syntax : mydeploy.sh <project> [<package>]"
	echo "sample : mydeploy.sh untitled org.qtproject.example.untitled"
	exit 1
fi

PROJECT="$1"
FILE="lib$1_armeabi-v7a.so"
PACKAGE="org.qtproject.example.$1"

if [ ! -z "$2" ]; then
	PACKAGE="$2"
fi

FOLDER="/data/data/$PACKAGE/lib"

echo "PROJECT="$PROJECT
echo "FILE="$FILE
echo "PACKAGE="$PACKAGE
echo "FOLDER="$FOLDER

adb push $FILE /data/local/tmp
adb exec-out "su -c 'cp /data/local/tmp/$FILE $FOLDER'"
adb exec-out "su -c 'chmod 755 $FOLDER/$FILE'"

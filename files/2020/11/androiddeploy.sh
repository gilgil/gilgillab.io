#!/bin/bash

if [ -z "$2" ]; then
	echo "syntax : androiddeploy.sh <FILE> <FOLDER>"
	echo "sample : androiddeploy.sh libuntitled_armeabi-v7a.so /data/data/org.qtproject.example.untitled/lib"
	exit 1
fi

FILE="$1"
FOLDER="$2"

adb push $FILE /data/local/tmp
adb exec-out "su -c 'cp /data/local/tmp/$FILE $FOLDER'"
adb exec-out "su -c 'chmod 755 $FOLDER/$FILE'"
